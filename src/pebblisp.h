#ifndef PEBBLISP_H
#define PEBBLISP_H

#include <stdio.h>

#include "env.h"
#include "object.h"

#define trueObject() boolObject(1)

#define falseObject() boolObject(0)

struct Slice {
    const char* text;
    unsigned char length;
    int lineNumber;
};

typedef struct Result {
    Object obj;
    struct Slice* slices;
} Result;

Object eval(const Object* obj, struct Environment* env);

Result readSeq(struct Slice* slices);

Result parseAtom(struct Slice* slice);

Object parseEval(const char* input, const char* fileName, struct Environment* env);

Object evalList(const Object* obj, struct Environment* env);

Object listEvalLambda(Object* lambda, const Object* passedArguments, int evalLength,
                      struct Environment* env);

Object funcyEval(Object* funcy, const Object* passedArguments, int evalLength,
                 struct Environment* env);

Object typeCheck(const char* funcName, Object* params, int length,
                 struct TypeCheck typeChecks[], int typeLength, int* failed);

#ifndef STANDALONE
#define DISABLE_TYPE_CHECKS
#endif

#ifndef DISABLE_TYPE_CHECKS
#define checkTypes(FUNC) int FAILED; Object ERROR = typeCheck(FUNC ## Symbol, params, length, FUNC ## TypeChecks, array_length(FUNC ## TypeChecks), &FAILED); \
if (FAILED) { \
    return ERROR; \
} else do { } while (0)
#define verifyTypes(FUNC, TYPE_CHECKS) int FAILED; Object ERROR = typeCheck(FUNC ## Symbol, params, length, TYPE_CHECKS, length, &FAILED); \
if (FAILED) { \
    return ERROR; \
}
#else
#define checkTypes(FUNC) do { } while (0)
#endif

#ifdef STANDALONE

char* readFileToString(FILE* input);

int _readFile(FILE* input, const char* fileName, struct Environment* env);

int readFile(const char* fileName, struct Environment* env);

struct Slice* getLastOpen();

#endif /* STANDALONE */

tfn(mapO, "map",
    ({ expect(isFuncy), expect(isListy), returns(isListy) }),
    "Map over a list with a function.",
    "(map (fn (a) (* a a)) (1 2 3))", "( 1 4 9 )",
);

fn(def, "def",
   "Define a variable in the current scope.",
   "(def x 10) x", "10",
);

fn(set, "set",
   "Set the value for a variable.\n"
   "Only defines a new variable if an existing one cannot be found.",
   "(set x 10) x", "10",
);

tfn(structAccess, "poss",
    ({ expect(isStruct), anyType, anyType }),
    "Get the value of a struct's field",
    "(struct Post (title body))\n    "
    "(def p (Post \"This is a title\" \"This is the body\"))\n      "
    "p.title", "This is a title"
);

#endif
