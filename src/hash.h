#ifndef PEBBLISP_HASH_H
#define PEBBLISP_HASH_H

#include "object.h"

struct ObjectTable {
    size_t count;
    size_t capacity;
    struct EnvElement* elements;
};

struct ObjectTable buildTable(size_t capacity);

/// Hash the given string
/// \param str The string to hash
/// \param table The table the hash should be generated for (currently ignored by the actual hashing implementation)
/// \return The hash value of the given string
size_t hash(const char* str, const struct ObjectTable* table);

/// \param table Where to insert the object
/// \param name Should be a new allocation
/// \param object Should already be cloned
/// \return The hash value of the name used to insert
size_t addToTable(struct ObjectTable* table, char* name, Object object);

/// TODO
struct StrippedObject* getWithHash(struct ObjectTable* table, const char* name, size_t hash);

struct StrippedObject* getFromTable(struct ObjectTable* table, const char* name);

void deleteTable(struct ObjectTable* table);

#ifdef STANDALONE

int isHash(const Object test);

struct ObjectTableObject {
    struct ObjectTable table;
    int refs;
};

fn(buildHashTable, "table",
   "Create a hash table object.\n",
   "(def t (table))\n  "
   "(h-insert t \"key\" 12345)\n  "
   "(h-get t \"key\")", /* => */ "12345"
);

tfn(addToHashTable, "h-insert",
    ({ expect(isHash), expect(isStringy), anyType, anyType }),
    "Insert into a hash table object.\n"
    "See (table)\n\n"
    "(h-insert my-table \"destination\" value)"
);

tfn(getFromHashTable, "h-get",
   ({ expect(isHash), expect(isStringy), anyType }),
   "Get a value from a hash table object.\n"
   "See (table)\n\n"
   "(h-get my-table \"mykey\") => myvalue"
);

#endif

#endif //PEBBLISP_HASH_H
