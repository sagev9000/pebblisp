#define _GNU_SOURCE // For segfault handling

#include "pebblisp.h"

#include <signal.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <unistd.h>

struct Settings {
    int runTests;
    long specificTest;
    int ignoreConfig;
    int ignoreLib;
    int moreToDo;
    const char* configFile;
    char historyFile[128];
} settings;

Object getPrompt(struct Environment* env)
{
    Object prompt = fetchFromEnvironment("prompt", env);
    if (!isError(prompt, DID_NOT_FIND_SYMBOL)) {
        prompt = cloneObject(prompt);
    } else {
        prompt = stringFromSlice("pl ~> ", 7);
    }
    if (prompt.type == TYPE_STRING) {
        return prompt;
    }
    Object param = stringFromSlice("", 1);
    Object e = funcyEval(&prompt, &param, 1, env);
    cleanObject(&prompt);
    cleanObject(&param);
    return e;
}

char* prompt(struct Environment* env)
{
    Object p = getPrompt(env);
    char* ret = readline(p.string);
    cleanObject(&p);
    return ret;
}

char* preprocess(char* buf, struct Environment* env)
{
    Object lambda = fetchFromEnvironment("preprocess", env);
    Object buffer = nullTerminated(buf);
    Object s = funcyEval(&lambda, &buffer, 1, env);
    size_t length;
    return stringObj(&s, &length);
}

void sigintHandler(unused int signalNumber)
{
    Object p = getPrompt(global());
    write(1, "\n", 1);
    write(1, p.string, strlen(p.string));
    cleanObject(&p);
}

char* completionGenerator(const char* text, int state)
{
    static size_t i;
    static size_t completionLength;
    if (state == 0) {
        i = 0;
        completionLength = strlen(text);
    }

    struct ObjectTable* table = &global()->table;
    for (; i < table->capacity; i++) {
        if (table->elements[i].symbol && strncmp(table->elements[i].symbol, text, completionLength) == 0) {
            char* symbol = strdup(table->elements[i].symbol);
            i++;
            return symbol;
        }
    }
    return NULL;
}

char** completer(const char* text, int start, int end)
{
    return rl_completion_matches(text, completionGenerator);
}

void repl(struct Environment* env)
{
    rl_attempted_completion_function = completer;
    signal(SIGINT, sigintHandler);
    char* buf;
    using_history();
    read_history(settings.historyFile);

    while ((buf = prompt(env)) != NULL) {
        buf = preprocess(buf, env);
        if (strcmp("q", buf) == 0) {
            free(buf);
            break;
        }
        if (buf[0] == '\0') {
            free(buf);
            continue;
        }
        add_history(buf);

        int isCd = strncmp("cd ", buf, 3) == 0;
        int isHelp = buf[0] == '?' && (buf[1] == ' ' || buf[1] == '\0');
        if (isCd || isHelp) {
            char* oldBuf = buf;
            if (isCd) {
                buf = malloc(sizeof(char) * strlen(buf + 3) + 8);
                sprintf(buf, "(cd \"%s\")", oldBuf + 3);
            } else {
                buf = malloc(sizeof(char) * (strlen(buf) + 3));
                sprintf(buf, "(%s)", oldBuf);
            }
            free(oldBuf);
        }

        Object o = parseEval(buf, "REPL", env);
        if (isFuncy(o) || isError(o, DID_NOT_FIND_SYMBOL)) {
            system(buf);
        } else {
            size_t length;
            char* output = stringObj(&o, &length);
            printColored(output);
            free(output);
            printf("[0m\n");
        }
        write_history(settings.historyFile);
        cleanObject(&o);
        free(buf);
    }
}

void loadArgsIntoEnv(int argc, const char* argv[], struct Environment* env)
{
    Object BuildListNamed(args);
    for (int i = 0; i < argc; i++) {
        addToList(args, nullTerminated(argv[i]));
    }
    addToEnv(env, "args", args);
}

#ifdef __x86_64__

#include <ucontext.h>

int nestedSegfault = 0;

void segfaultHandler(unused int nSignum, unused siginfo_t* si, void* vcontext)
{
    if (nestedSegfault) {
        printf("Nested segfault!!!\n");
        exit(139);
    }
    nestedSegfault = 1;

    printf("Segfaulted!\n");
    struct Slice* lastOpen = getLastOpen();
    if (lastOpen) {
        printf("line: %d\n%s\n", lastOpen->lineNumber, lastOpen->text);
    } else {
        printf("Happened before token processing.\n");
    }
    ucontext_t* context = vcontext;
    context->uc_mcontext.gregs[REG_RIP]++;
    exit(139);
}

void setupSegfaultHandler()
{
    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_flags = SA_SIGINFO;
    action.sa_sigaction = segfaultHandler;
    sigaction(SIGSEGV, &action, NULL);
}

#else
void setupSegfaultHandler()
{
}
#endif

#define SPECIFIC_TEST_ARG "--run-test"
#define RUN_TESTS_ARG "--run-tests"
#define RUN_DETAILED_TESTS "=detailed"
#define IGNORE_CONFIG_ARG "--ignore-config"
#define IGNORE_LIB_ARG "--ignore-lib"
#define CONFIG_FILE_ARG "--config="

void getSettings(int argc, const char* argv[])
{
    const char* const home = getenv("HOME");

    settings.runTests = 0;
    settings.specificTest = -1;
    settings.ignoreConfig = 0;
    settings.ignoreLib = 0;
    settings.moreToDo = 0;
    settings.configFile = NULL;

    sprintf(settings.historyFile, "%s/.plhist", home);

    size_t runTestsLen = strlen(RUN_TESTS_ARG);
    size_t configFileLen = strlen(CONFIG_FILE_ARG);
    size_t specificTestLen = strlen(SPECIFIC_TEST_ARG);
    for (int i = 1; i < argc; i++) {
        if (strncmp(argv[i], RUN_TESTS_ARG, runTestsLen) == 0) {
            int isDetailed = strcmp(argv[i] + runTestsLen, RUN_DETAILED_TESTS) == 0;
            settings.runTests = isDetailed ? 2 : 1;
        } else if (strncmp(argv[i], CONFIG_FILE_ARG, configFileLen) == 0) {
            settings.configFile = argv[i] + configFileLen;
        } else if (strncmp(argv[i], SPECIFIC_TEST_ARG, specificTestLen) == 0) {
            settings.runTests = 2;
            char* invalid;
            settings.specificTest = strtol(argv[i + 1], &invalid, 10);
        } else if (strcmp(argv[i], IGNORE_CONFIG_ARG) == 0) {
            settings.ignoreConfig = 1;
        } else if (strcmp(argv[i], IGNORE_LIB_ARG) == 0) {
            settings.ignoreLib = 1;
        } else if (argv[i][0] == '-') {
            fprintf(stderr, "Unrecognized argument: '%s'\n", argv[i]);
        } else if (i == (argc - 1)) {
            settings.moreToDo = 1;
        }
    }
}

int main(int argc, const char* argv[])
{
    setupSegfaultHandler();
    getSettings(argc, argv);

    struct Environment env = defaultEnv();
    setGlobal(&env);

    if (settings.runTests) {
        int ret = runTests(settings.runTests == 2, settings.specificTest);
        shredDictionary();
        deleteEnv(global());
        return ret;
    }

    if (!settings.ignoreLib) {
        readFile(SCRIPTDIR "/lib.pbl", &env);
    }

    Object o = parseEval("(def prompt %%)", "[INTERNAL]", &env);
    cleanObject(&o);
    o = parseEval("(def preprocess (fn (text) (text)))", "[INTERNAL]", &env);
    cleanObject(&o);

    if (!settings.ignoreConfig) {
        char config[128];
        if (settings.configFile) {
            sprintf(config, "%s", settings.configFile);
        } else {
            sprintf(config, "%s/.pebblisp.pbl", getenv("HOME"));
        }
        if (readFile(config, &env) == 1 && settings.configFile) {
            fprintf(stderr, "Config file not found at %s\n", config);
        }
    }

    if (settings.moreToDo) {
        FILE* file = fopen(argv[argc - 1], "r");
        if (file) {
            // Execute a file
            loadArgsIntoEnv(argc, argv, &env);
            _readFile(file, argv[argc - 1], &env);
        } else {
            // Run arguments directly as pl code
            Object r = parseEval(argv[argc - 1], "[PL ARGUMENTS]", &env);
            printAndClean(&r);
        }
    } else {
        // Run a repl
        loadArgsIntoEnv(argc, argv, &env);
        repl(&env);
    }
    deleteEnv(&env);
    shredDictionary();
    // eprintf("totalSearchDepth: %d of %d searches\n", getTotalSearchDepth(), getTotalSearches());
    // eprintf("\nHEAP-ALLOCATED OBJECTS: %d\n", getAllocations());
    // eprintf("TOTAL OBJECT.C ALLOC: %zu\n", getBytes());
}