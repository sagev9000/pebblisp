#include "env.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pebblisp.h"
#include "plfunc/web.h"
#include "plfunc/threads.h"
#include "plfunc/general.h"
#include "plfunc/pc.h"
#include "plfunc/plstring.h"

struct symFunc buildFuncSym(const char* symbol, Object (* func)(Object*, int, struct Environment*), const char* help,
                            const char* const* tests, size_t testLength);

struct Environment* globalEnv;

struct Environment* global()
{
    return globalEnv;
}

void setGlobal(struct Environment* env)
{
    globalEnv = env;
}

struct Dictionary {
    int structCount;
    int structCapacity;
    struct StructDef* structDefs;
} dictionary;

/// For any instances (e.g. segfault recovery) where defaultEnv() may be called
/// multiple times.
int helpInitialized = 0;

#ifdef STANDALONE
struct helpText {
    const char* symbol;
    const char* help;
    const char* const* tests;
    size_t testCount;
};
int currentHelp = 0;
struct helpText helpTexts[100];
#endif

struct symFunc {
    const char* sym;
    Object (* func)(Object*, int, struct Environment*);
};

#ifdef STANDALONE
#define pf(_func) buildFuncSym(_func ## Symbol, &(_func), _func ## Doc, _func ## Tests, array_length(_func ## Tests))
#else
#define pf(_func) buildFuncSym(_func ## Symbol, &(_func))
#endif

struct StrippedObject* fetch(const char* name, struct Environment* env)
{
    while (env) {
        struct StrippedObject* object = getFromTable(&env->table, name);
        if (object) {
            return object;
        }
        env = env->outer;
    }
    return NULL;
}

Object fetchFromEnvironment(const char* name, struct Environment* env)
{
    struct StrippedObject* object = fetch(name, env);
    if (object) {
        Object o = deStrip(*object);
        return cloneObject(o);
    }
    throw(DID_NOT_FIND_SYMBOL, "%s", name);
}

void setVar(struct Environment* env, const char* name, const Object obj, int recurse)
{
    struct StrippedObject* existing = recurse
        ? fetch(name, env)
        : getFromTable(&env->table, name);

    if (existing) {
        Object o = deStrip(*existing);
        cleanObject(&o);
        o = cloneObject(obj);
        existing->type = o.type;
        existing->data = o.data;
        return;
    }
    addToTable(&env->table, strdup(name), cloneObject(obj));
}

void addToEnv(struct Environment* env, const char* name, const Object obj)
{
    setVar(env, name, obj, 0);
}

void setInEnv(struct Environment* env, const char* name, const Object obj)
{
    setVar(env, name, obj, 1);
}

/**
 * Build the local Environment for a given lambda
 * @param params List of symbol objects with the names of lambda params
 * @param paramCount Length of params list
 * @param arguments Arguments being passed into the lambda where ->forward is the next arg
 * @param outer The parent scope
 * @return The constructed environment
 */
struct Environment envForLambda(const Object* params, const Object* arguments, int paramCount,
                                struct Environment* outer)
{
    if (outer) {
        outer->refs += 1;
    }

    struct Environment env = {
        .outer = NULL,
        .table = buildTable(paramCount * 2),
        .refs = 1,
    };

    // Evaluate the `argument` list
    const Object* param = params->list;
    const Object* argument = arguments;
    for (; param; param = param->forward) {
        const char* paramName = param->string;
        if (paramName[0] == '.' && paramName[1] == '.' && paramName[2] == '.' && paramName[3] != '\0') {
            paramName = &paramName[3];
            Object BuildListNamed(varargs);
            while (argument) {
                addToList(varargs, eval(argument, outer));
                argument = argument->forward;
            }
            addToEnv(&env, paramName, varargs);
            cleanObject(&varargs);
            break;
        }
        if (!argument) {
            break;
        }

        Object newEnvObj = eval(argument, outer);

        addToEnv(&env, paramName, newEnvObj);

        cleanObject(&newEnvObj);

        argument = argument->forward;
    }
    env.outer = outer;

    return env;
}

void deleteEnv(struct Environment* e)
{
    e->refs -= 1;
    if (e->refs) {
        return;
    }

    if (e->outer) {
        deleteEnv(e->outer);
    }

    deleteTable(&e->table);
}


struct Environment defaultEnv()
{
#ifndef STANDALONE
    int helpInitialized = 0;
#endif
    if (!helpInitialized) {
        dictionary = (struct Dictionary) {
            .structCount = 0,
            .structCapacity = 8,
            .structDefs = malloc(sizeof(struct StructDef) * 8),
        };
    }

    struct Environment e = {
        .table = buildTable(128),
        .outer = NULL,
        .refs = 1,
    };

    struct symFunc symFuncs[] = {
        pf(def),
        pf(set),
        pf(add),
        pf(sub),
        pf(mul),
        pf(dvi),
        pf(mod),
        pf(equ),
        pf(greaterThan),
        pf(lessThan),
        pf(and),
        pf(or),
        pf(catObjects),
        pf(filter),
        pf(len),
        pf(append),
        pf(prepend),
        pf(reduce),
        pf(mapO),
        pf(at),
#ifndef PBL_PLATFORM_APLITE
        pf(rest),
        pf(charAt),
        pf(chars),
        pf(matches),
        pf(slen),
        pf(substring),
        pf(isNum),
        pf(isList),
        pf(isString),
        pf(isErr),
        pf(charVal),
        pf(parseEvalO),
#endif
        pf(structAccess),
        pf(getTime),
#ifndef LOW_MEM
        pf(reverse),
#endif
#ifdef WEBSERVER
        pf(addGetRoute),
        pf(addPostRoute),
        pf(startServer),
#endif
#ifdef STANDALONE
        pf(segfault),
        pf(typeOf),
        pf(print),
        pf(numToChar),
        pf(printEnvO),
        pf(systemCall),
        pf(cd),
        pf(cwd),
        pf(takeInput),
        pf(readFileToObject),
        pf(getEnvVar),
        pf(async),
        pf(await),
        pf(help),
        pf(buildHashTable),
        pf(addToHashTable),
        pf(getFromHashTable),
        pf(randomO)
#endif
    };

    for (unsigned i = 0; i < sizeof(symFuncs) / sizeof(symFuncs[0]); i++) {
        addFunc(symFuncs[i].sym, symFuncs[i].func, &e);
    }

    helpInitialized = 1;
    return e;
}

void printEnv(struct Environment* env, int printPointers)
{
    if (!env) {
        printf("NULL env\n");
        return;
    }
    printf("env->capacity = %lu\n", env->table.capacity);
    printf("env->count = %lu\n", env->table.count);

    for (int i = 0; i < env->table.capacity; i++) {
        printf("[0m");
        if (env->table.elements[i].symbol == NULL) {
            continue;
        }
        printf("%s: ", env->table.elements[i].symbol);
        printf("[0m");
        if (env->table.elements[i].object.type == TYPE_STRING) {
            printf("\"");
        }
        if (env->table.elements[i].object.type == TYPE_FUNC && !printPointers) {
            printf("Native");
        } else {
            size_t length;
            // SAFETY: Casting to Object is fine because stringObj() doesn't touch .forward
            char* s = stringObj((Object*) &env->table.elements[i].object, &length);
            printColored(s);
            if (env->table.elements[i].object.type == TYPE_STRING) {
                printf("\"");
            }
            free(s);
        }
        if (printPointers) {
            printf(" @ %p", env->table.elements[i].symbol);
        }
        printf("\n");
    }
}

void addFunc(const char* name,
             Object (* func)(Object*, int, struct Environment*),
             struct Environment* env)
{
    Object o = newObject(TYPE_FUNC);
    o.func = func;
    addToEnv(env, name, o);
}

void shredDictionary()
{
    for (int i = 0; i < dictionary.structCount; i++) {
        free(dictionary.structDefs[i].name);
        for (int j = 0; j < dictionary.structDefs[i].fieldCount; j++) {
            free(dictionary.structDefs[i].names[j]);
        }
        free(dictionary.structDefs[i].names);
    }
    free(dictionary.structDefs);
}

#ifdef STANDALONE

struct symFunc buildFuncSym(const char* symbol, Object (* func)(Object*, int, struct Environment*), const char* help,
                            const char* const* tests, size_t testLength)
{
    if (!helpInitialized) {
        helpTexts[currentHelp].help = help;
        helpTexts[currentHelp].symbol = symbol;
        helpTexts[currentHelp].tests = tests;
        helpTexts[currentHelp].testCount = testLength;
        currentHelp += 1;
    }

    return (struct symFunc) {
        .func = func,
        .sym = symbol,
    };
}

#else
struct symFunc buildFuncSym(const char* symbol, Object (* func)(Object*, int, struct Environment*))
{
    return (struct symFunc) {
        .func = func,
        .sym = symbol,
    };
}
#endif

#ifdef STANDALONE
const int black = 30;
const int red = 31;
const int green = 32;
const int yellow = 33;
const int cyan = 34;
const int purple = 35;
const int teal = 36;
const int white = 37;
const int colors[] = {
    white, green, cyan, yellow, purple, red
};
const int colorCount = array_length(colors);

int getColor(int depth)
{
    depth = depth >= 0 ? depth : depth * -1;
    return colors[depth % colorCount];
}

void printColored(const char* code)
{
    int c = 0;
    int depth = 0;
    int isQuote = 0;
    printf("[%dm", getColor(depth));
    while (code[c]) {
        if ((code[c] == '(' || code[c] == '{') && !isQuote) {
            depth += 1;
            printf("[%dm", getColor(depth));
        } else if ((code[c] == ')' || code[c] == '}') && !isQuote) {
            depth -= 1;
            printf("%c[%dm", code[c], getColor(depth));
            c++;
            continue;
        } else if (code[c] == '"') {
            isQuote = !isQuote;
            if (isQuote) {
                printf("[%dm\"", teal);
            } else {
                printf("\"[%dm", getColor(depth));
            }
            c++;
            continue;
        } else if (code[c] == '=' && code[c + 1] == '>' && !isQuote) {
            printf("[1m=>[0;%dm", getColor(depth));
            c += 2;
            continue;
        }
        printf("%c", code[c]);
        c++;
    }
}

Object segfault(Object* params, int length, struct Environment* env)
{
    int* p = NULL;
    return numberObject(*p);
}

Object help(Object* params, int length, struct Environment* env)
{
    const char* symbol;
    if (!length || !params[0].string || params[0].string[0] == '\0') {
        symbol = "?";
    } else {
        symbol = params[0].string;
    }
    for (int i = 0; i < currentHelp; i++) {
        struct helpText h = helpTexts[i];
        if (strcmp(symbol, h.symbol) != 0) {
            continue;
        }
        Object text = withLen(1024, TYPE_STRING);
        char* textCursor = text.string;
        textCursor += sprintf(textCursor, "%s", h.help);
        for (int ti = 0; ti < h.testCount; ti += 2) {
            const char* test = h.tests[ti];
            const char* expected = h.tests[ti + 1];
            if (test[0] == ';') {
                textCursor += sprintf(textCursor, "\n  %s => %s", test + 1, expected);
                continue;
            }
            textCursor += sprintf(textCursor, "\n  %s => ", test);
            if (expected[0] == '\0') {
                textCursor += sprintf(textCursor, "<empty string>");
            } else {
                textCursor += sprintf(textCursor, "%s", expected);
            }
        }
        return text;
    }

    for (int i = 0; i < dictionary.structCount; i++) {
        if (strcmp(dictionary.structDefs[i].name, symbol) == 0) {
            char structDef[128] = { '{', ' ', '\0' };
            for (int field = 0; field < dictionary.structDefs[i].fieldCount; field++) {
                strcat(structDef, dictionary.structDefs[i].names[field]);
                strcat(structDef, " ");
            }
            strcat(structDef, "}");
            return nullTerminated(structDef);
        }
    }

    struct StrippedObject* object = fetch(symbol, env);
    if (object) {
        size_t len = 0;
        // SAFETY: Casting to Object is fine because stringObj() doesn't touch .forward
        char* string = stringObj((Object*) object, &len);
        Object text = stringFromSlice(string, len);
        free(string);
        return text;
    }

    return nullTerminated("Help not found!");
}

/// Returns 1 if the test passed, or 0 if it failed
int runTest(const char* test, const char* expected, int detailed)
{
    struct Environment env = defaultEnv();
    Object o = parseEval(test, "testCode", &env);
    size_t length;
    char* result = stringObj(&o, &length);
    cleanObject(&o);

    int expectedLen = 0; // Don't count anything after a semicolon
    while (expected[expectedLen] && expected[expectedLen] != ';') {
        expectedLen++;
    }

    int ret;
    if (strncmp(result, expected, expectedLen) != 0) {
        ret = 0;
        printf("Test failed!\n");
        printf("%s\n", test);
        printf("Expected '%s' but received '%s'\n", expected, result);
    } else {
        ret = 1;
        if (detailed) {
            printf("[32;1m✓");
        }
    }
    free(result);
    deleteEnv(&env);
    return ret;
}

void runHelpTests(struct helpText h, int detailed, int* failureCount, int* passCount)
{
    if (!h.tests) {
        return;
    }
    if (detailed && h.testCount > 0) {
        printf("[0m `%s` ", h.symbol);
    }
    for (int ti = 0; ti < h.testCount; ti += 2) {
        const char* test = h.tests[ti];
        const char* expected = h.tests[ti + 1];
        if (test[0] == ';') {
            continue;
        }
        int* outcome = runTest(test, expected, detailed) ? passCount : failureCount;
        *outcome = *outcome + 1;
    }
    if (detailed && h.testCount > 0) {
        printf("\n");
    }
}

// Returns number of failures, or -1 if the requested specificTest is invalid
int runTests(int detailed, int specificTest)
{
    if (specificTest >= currentHelp) {
        return -1;
    }

    int failureCount = 0;
    int passCount = 0;

    int isSpecificTest = specificTest >= 0;
    if (isSpecificTest) {
        struct helpText h = helpTexts[specificTest];
        runHelpTests(h, detailed, &failureCount, &passCount);
        return failureCount;
    }

    printf("[35;1m::NATIVE TESTS::[0m\n");

    for (int hi = 0; hi < currentHelp; hi++) {
        runHelpTests(helpTexts[hi], detailed, &failureCount, &passCount);
    }

    if (passCount > 0) {
        printf("[32;1m");
    }
    printf("%d tests passed![0m\n", passCount);
    if (failureCount > 0) {
        printf("[31m%d tests failed![0m\n", failureCount);
    }

    // fprintf(stderr, "TOTAL ALLOCATIONS: %d\n", getAllocations());
    // fprintf(stderr, "TOTAL BYTES: %zu\n", getBytes());

    return failureCount;
}

#endif

int getStructIndex(const char* name)
{
    for (int i = 0; i < dictionary.structCount; i++) {
        if (strcmp(name, dictionary.structDefs[i].name) == 0) {
            return i;
        }
    }
    return -1;
}

struct StructDef* getStructAt(int i)
{
    return &dictionary.structDefs[i];
}

void addStructDef(struct StructDef def)
{
    dictionary.structDefs[dictionary.structCount] = def;
    dictionary.structCount += 1;
    if (dictionary.structCount == dictionary.structCapacity) {
        struct StructDef* prev = dictionary.structDefs;
        int prevCapacity = dictionary.structCapacity;
        dictionary.structCapacity *= 2;
        dictionary.structDefs = malloc(sizeof(struct StructDef) * dictionary.structCapacity);
        for (int i = 0; i < prevCapacity; i++) {
            dictionary.structDefs[i] = prev[i];
        }
        free(prev);
    }

}
