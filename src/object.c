#include "object.h"
#include "env.h"

#include <stdio.h>
#include <string.h>

#ifdef STANDALONE

#include "plfunc/threads.h"

size_t bytes = 0;

size_t getBytes()
{
    return bytes;
}

#undef malloc
#undef calloc

void* smalloc(size_t size)
{
    bytes += size;
    return malloc(size);
}

void* scalloc(size_t size, size_t count)
{
    bytes += (size * count);
    return calloc(size, count);

}

#define malloc(x) smalloc(x)
#define calloc(x, y) scalloc(x, y)
#endif

/**
 * Returns the length of a given list Object
 * @param listObj The list to get the length of
 * @return Length of the list if non-null and of the list type. Otherwise -1
 */
int listLength(const Object* listObj)
{
    assert(isListy(*listObj));
    int len = 0;
    FOR_POINTER_IN_LIST(listObj) {
        len++;
    }
    return len;
}

/**
 * Returns a pointer to the last Object in the given list.
 * @param listObj The list to find the tail of.
 * @return A pointer to the last Object if it is found, or NULL on an error
 */
Object* tail(const Object* listObj)
{
    assert(isListy(*listObj));
    Object* tail = NULL;
    FOR_POINTER_IN_LIST(listObj) {
        tail = POINTER;
    }
    return tail;
}

int allocations = 0;

int getAllocations()
{
    return allocations;
}

/**
 * Allocate a copy of a given object into the given pointer.
 * Does nothing if `spot` is NULL
 * @param spot A pointer to the Object pointer that needs allocating
 * @param src The Object to copy from
 */
void allocObject(Object** spot, const Object src)
{
#ifdef ALLOCATION_CAP
    if (allocations >= 10000) {
        printf("MAX ALLOCATIONS EXCEEDED\n");
        *spot = NULL;
        return;
    }
#endif
    allocations++;
    *spot = malloc(sizeof(struct Object));
    **spot = src;
    (*spot)->forward = NULL;
}

/**
 * Adds an Object to the end of a list
 * Does nothing if `dest` is NULL or not a list type
 * @param dest The list to append to
 * @param src The Object to copy into the list
 * @returns A pointer to the tail of the destination list
 */
Object* nf_addToList(Object* dest, const Object src)
{
    if (dest->list == NULL) {
        allocObject(&dest->list, src);
        return dest->list;
    }

    Object* listTail = tail(dest);
    allocObject(&listTail->forward, src);
    return listTail->forward;
}

#ifndef SIMPLE_ERRORS
static const char* errorText[] = {
    "MISMATCHED_PARENS",
    "NULL_ENV",
    "EMPTY_ENV",
    "NULL_PARSE",
    "NULL_LAMBDA_LIST",
    "NULL_MAP_ARGS",
    "LAMBDA_ARGS_NOT_LIST",
    "DID_NOT_FIND_SYMBOL",
    "BAD_SYMBOL",
    "BAD_TYPE",
    "BAD_PARAMS",
    "BAD_NUMBER",
    "UNSUPPORTED_NUMBER_TYPE",
    "NOT_ENOUGH_ARGUMENTS",
    "NOT_A_LIST",
    "SCRIPT_NOT_FOUND",
    "NO_CLONE_SPECIFIED",
    "CAN_ONLY_EVAL_STRINGS",
    "UNEXPECTED_EOF",
    "INDEX_PAST_END" };
#endif

size_t inflate(struct string* s, size_t additional)
{
    size_t length = (s->cursor - s->allocPoint) + additional;

    int needsInflation = 0;
    while (length > s->capacity - 8) {
        s->capacity *= 2;
        needsInflation = 1;
    }

    if (needsInflation) {
        char* oldAlloc = s->allocPoint;
        s->allocPoint = malloc(sizeof(char) * s->capacity);
        size_t l = sprintf(s->allocPoint, "%s", oldAlloc);
        s->cursor = s->allocPoint + l;
        free(oldAlloc);
    }

    return s->capacity;
}

#define appendf(STRING, ...) (STRING)->cursor += sprintf((STRING)->cursor, __VA_ARGS__)

int stringNObj(struct string* s, const Object* obj);

/**
 * Creates a string from a given list Object
 * Blocks out with parens
 * Returns immediately if `dest` is NULL, or `obj` is not a list type
 * @param dest The string to copy the list text into
 * @param obj The list Object to make a string from
 */
void stringList(struct string* s, const Object* obj)
{
    appendf(s, "(");

    FOR_POINTER_IN_LIST(obj) {
        appendf(s, " ");
        stringNObj(s, POINTER);
        inflate(s, 0);
    }
    appendf(s, " )");
}

/**
 * Creates a string from a given struct Object
 * Blocks out with curly braces
 */
void stringStruct(struct string* s, const Object* obj)
{
    struct StructObject* so = obj->structObject;
    appendf(s, "{");

    struct StructDef* def = getStructAt(so->definition);
    for (int i = 0; i < def->fieldCount; i++) {
        appendf(s, " %s: ", def->names[i]);
        int isString = so->fields[i].type == TYPE_STRING;
        if (isString) {
            appendf(s, "\"");
        }

        stringNObj(s, &so->fields[i]);

        if (isString) {
            appendf(s, "\"");
        }
        appendf(s, ",");
    }
    s->cursor--;
    appendf(s, " }");
}

/**
 * Creates a string from a given Object
 * Returns NULL if either param is NULL
 *
 * Prints
 * -# Numbers without spaces
 * -# Symbols as their name
 * -# Bools as 'T' or 'F'
 * -# Strings as their string value
 * -# Lists using stringList()
 * -# Errors as the integer value, prepended with 'E'
 * -# Otherwise as the raw number, prepended with 'X'
 *
 * @param dest The string to copy the list text into
 * @param obj The list Object to make a string from
 */
int stringNObj(struct string* s, const Object* obj)
{
    inflate(s, 32); // In most cases this is enough to fit the appended string.
    switch (obj->type) {
        case TYPE_NUMBER:
            appendf(s, "%ld", obj->number);
            break;
        case TYPE_BOOL:
            appendf(s, "%s", obj->number ? "T" : "F");
            break;
        case TYPE_SYMBOL:
        case TYPE_STRING: {
            size_t stringLen = strlen(obj->string);
            inflate(s, stringLen);
            const char* format = obj->type == TYPE_STRING ? "%s" : "`%s`";
            appendf(s, format, obj->string);
            break;
        }
        case TYPE_STATIC_FUNC:
            appendf(s, "STATIC_FUNC");
            break;
        case TYPE_HASH_TABLE:
            appendf(s, "HASH_TABLE");
            break;
        case TYPE_STRUCT:
            stringStruct(s, obj);
            break;
        case TYPE_SLIST:
            appendf(s, "'");
        case TYPE_LIST:
            stringList(s, obj);
            break;
        case TYPE_PROMISE:
            appendf(s, isDone(obj->promise) ? "<DONE>" : "<PENDING>");
            break;
        case TYPE_ERROR: {
            int code = getErrorCode(*obj);
#ifdef SIMPLE_ERRORS
            appendf(s, "E[%d]", (int)code);
#else
            inflate(s, 128);
            if (obj->error->context && obj->error->context[0] != '\0') {
                appendf(s, "%s: %s", errorText[code],
                        obj->error->context);
            } else if (code >= 0 && code <= INDEX_PAST_END) {
                appendf(s, "%s", errorText[code]);
            } else {
                appendf(s, "BROKEN ERROR CODE: %d", code);
            }
#endif
            break;
        }
        case TYPE_FUNC:
            appendf(s, "NATIVE_FUNC");
            break;
        case TYPE_LAMBDA: {
#if defined(STANDALONE) && !defined(DEBUG)
            char** docStrings = lambdaDocs(obj);
            if (docStrings) {
                for (; *docStrings; docStrings++) {
                    inflate(s, strlen(*docStrings));
                    appendf(s, "%s\n", *docStrings);
                }
            }
            stringNObj(s, &obj->lambda->params);
            appendf(s, " -> ");
            stringNObj(s, &obj->lambda->body);
            appendf(s, ">");
#else
            appendf(s, "\\x%d", obj->number);
#endif
            break;
        }
        case TYPE_OTHER:
            appendf(s, "%p", obj->other->data);
            break;
    }

    if (!isValidType(*obj)) {
        appendf(s, "BAD_TYPE(%d) X%ld", obj->type, obj->number);
    }

    return 0;
}

char* stringObj(const Object* obj, size_t* length)
{
    char* alloc = malloc(8);
    struct string s = {
        .allocPoint = alloc,
        .cursor = alloc,
        .capacity = 8,
    };
    s.allocPoint[0] = '\0';
    stringNObj(&s, obj);
    *length += s.cursor - s.allocPoint;
    return s.allocPoint;
}

#if defined(DEBUG) || defined(STANDALONE)

#define SIMPLE_TYPE(_type) case _type:\
    return #_type;

const char* getTypeName(const Object* obj)
{
    if (!obj) {
        return "NULL_OBJECT";
    }
    switch (obj->type) {
        SIMPLE_TYPE(TYPE_NUMBER);
        SIMPLE_TYPE(TYPE_STRUCT);
        SIMPLE_TYPE(TYPE_BOOL);
        SIMPLE_TYPE(TYPE_LIST);
        SIMPLE_TYPE(TYPE_SLIST);
        SIMPLE_TYPE(TYPE_FUNC);
        SIMPLE_TYPE(TYPE_STATIC_FUNC);
        SIMPLE_TYPE(TYPE_SYMBOL);
        SIMPLE_TYPE(TYPE_STRING);
        SIMPLE_TYPE(TYPE_HASH_TABLE);
        SIMPLE_TYPE(TYPE_PROMISE);
        SIMPLE_TYPE(TYPE_OTHER);
        SIMPLE_TYPE(TYPE_ERROR);
        SIMPLE_TYPE(TYPE_LAMBDA);
    }

    return "UNKNOWN_TYPE";
}

void _printObj(const Object* obj, int newline)
{
    if (!obj) {
        printf(newline ? "<null>\n" : "<null>");
        return;
    }
    size_t length;
    char* temp = stringObj(obj, &length);
    if (newline) {
        printf("%s\n", temp);
        if (obj->type == TYPE_ERROR) {
            if (obj->error && obj->error->plContext) {
                printf("%s\n", obj->error->plContext->text);
            }
        }
    } else {
        printf("%s", temp);
    }
    free(temp);
}

/**
 * Prints the given Object and a newline.
 * Uses stringObj() to create the printed string
 * @param obj The Object to print
 */
inline void printObj(const Object* obj)
{
    _printObj(obj, 1);
}

#endif

/**
 * Performs appropriate free() on a given Object and NULLs its ->forward
 *
 * Strings: The Object's ->string is freed
 * Lists: deleteList() is called on the Object (may recurse)
 * Lambdas: The body and params are cleaned, and the lambda itself is freed
 *
 * @param target The object to clean
 */
void cleanObject(Object* target)
{
    switch (target->type) {
        case TYPE_STRING:
        case TYPE_SYMBOL:
            if ((stringRefs(target) -= 1) == 0) {
                free(target->string - 1);
            }
            return;
        case TYPE_LIST:
        case TYPE_SLIST:
            deleteList(target);
            return;
        case TYPE_HASH_TABLE:
            target->table->refs -= 1;
            if (!target->table->refs) {
                deleteTable(&target->table->table);
                free(target->table);
            }
            return;
        case TYPE_LAMBDA:
            lambdaRefs(target) -= 1;
            if (!lambdaRefs(target)) {
                cleanObject(&target->lambda->params);
                cleanObject(&target->lambda->body);
                char** docTexts = lambdaDocs(target);
                if (docTexts) {
                    while (*docTexts) {
                        free(*docTexts);
                        docTexts++;
                    }
                    free(lambdaDocs(target));
                }
                free(target->lambda);
            }
            return;
        case TYPE_STRUCT:
            for (int i = 0; i < getStructAt(target->structObject->definition)->fieldCount; i++) {
                cleanObject(&target->structObject->fields[i]);
            }
            free(target->structObject->fields);
            free(target->structObject);
            return;
        case TYPE_PROMISE:
            cleanPromise(target->promise);
            return;
        case TYPE_ERROR:
#ifndef SIMPLE_ERRORS
            free(target->error->plContext);
            free(target->error->context);
            free(target->error);
#endif
            return;
        case TYPE_OTHER:
            if (target->other->cleanup) {
                target->other->cleanup(target);
            }
            return;
        case TYPE_STATIC_FUNC:
            if ((target->staticF->refs -= 1) == 0) {
                for (int i = 0; i < target->staticF->argCount; i++) {
                    cleanObject(&target->staticF->arguments[i]);
                }
                free(target->staticF);
            }
            return;
        case TYPE_BOOL:
        case TYPE_NUMBER:
        case TYPE_FUNC:
            return;
    }
}

/**
 * Print the given object with a newline, then clean it
 * @param target The object to print and clean
 */
#ifdef STANDALONE

void printAndClean(Object* target)
{
    printObj(target);
    cleanObject(target);
}

#endif

/**
 * Frees all objects in a given list
 * Runs cleanObject() on every item in the list (may recurse)
 *
 * @param dest The list object to clean up
 */
void deleteList(Object* dest)
{
    assert(isListy(*dest));

    Object* march = dest->list;
    while (march != NULL) {
        Object* prevMarch = march;
        march = march->forward;
        cleanObject(prevMarch);
        free(prevMarch);
    }
}

/**
 * Does a deep copy of all items from `src` to `dest`
 * Does nothing if either param is NULL, or not a list type
 * Does a shallow delete of items from `dest` before copying
 * May recurse into lists in the list
 *
 * @param dest The list to copy to
 * @param src The list to copy from
 */
Object cloneList(const Object* src)
{
    assert(isListy(*src));

    Object BuildListNamed(list);
    FOR_POINTER_IN_LIST(src) {
        addToList(list, cloneObject(*POINTER));
    }
    list.type = src->type;
    return list;
}

/**
 * Returns a basic Object with NULL forward and the given type
 * @param type The type of Object to create
 * @return The created Object
 */
inline Object newObject(Type type)
{
    Object no;
    no.forward = NULL;
    no.type = type;
    return no;
}

// Returns an empty list Object
inline Object listObject()
{
    Object list = newObject(TYPE_LIST);
    list.list = NULL;
    return list;
}

// Returns an empty struct Object
inline Object structObject(int definition)
{
    Object structo = newObject(TYPE_STRUCT);
    structo.structObject = malloc(sizeof(struct StructObject));
    structo.structObject->definition = definition;
    structo.structObject->fields = malloc(sizeof(Object) * getStructAt(definition)->fieldCount);
    return structo;
}

// Returns a list Object starting with the given Object
inline Object startList(const Object start)
{
    Object list = listObject();
    nf_addToList(&list, start);
    return list;
}

inline int isNumber(const Object test)
{
    return test.type == TYPE_NUMBER;
}

inline int isListy(const Object test)
{
    return test.type == TYPE_LIST || test.type == TYPE_SLIST;
}

inline int isStringy(const Object test)
{
    return test.type == TYPE_STRING || test.type == TYPE_SYMBOL;
}

inline int isBool(const Object test)
{
    return test.type == TYPE_BOOL;
}

inline int isStruct(const Object test)
{
    return test.type == TYPE_STRUCT;
}

inline int isFuncy(const Object test)
{
    return test.type == TYPE_LAMBDA || test.type == TYPE_FUNC;
}

inline int isValidType(const Object test)
{
    return test.type >= 0 && test.type <= TYPE_ERROR;
}

Object cloneString(Object obj)
{
    stringRefs(&obj) += 1;
    return obj;
}

Object cloneOther(const Object src)
{
    if (src.other->clone) {
        return src.other->clone(src.other);
    }

    return src;
}

Object cloneStruct(Object src);

inline Object cloneObject(const Object src)
{
    switch (src.type) {
        case TYPE_SLIST:
        case TYPE_LIST:
            return cloneList(&src);
        case TYPE_LAMBDA:
            lambdaRefs(&src) += 1;
            return src;
        case TYPE_STRUCT:
            return cloneStruct(src);
        case TYPE_STRING:
        case TYPE_SYMBOL:
            return cloneString(src);
        case TYPE_PROMISE:
            return clonePromise(src);
        case TYPE_ERROR:
            return errorWithContext(getErrorCode(src), src.error->context);
        case TYPE_OTHER:
            return cloneOther(src);
        case TYPE_HASH_TABLE:
            src.table->refs += 1;
            return src;
        case TYPE_STATIC_FUNC:
            src.staticF->refs += 1;
            return src;
        case TYPE_BOOL:
        case TYPE_NUMBER:
        case TYPE_FUNC:
            return src;
    }

    return src;
}

Object cloneStruct(const Object src)
{
    assert(src.type == TYPE_STRUCT);
    Object structo = structObject(src.structObject->definition);
    struct StructObject* so = structo.structObject;
    for (int i = 0; i < getStructAt(so->definition)->fieldCount; i++) {
        so->fields[i] = cloneObject(src.structObject->fields[i]);
    }
    return structo;
}

inline Object numberObject(int num)
{
    Object o = newObject(TYPE_NUMBER);
    o.number = num;
    return o;
}

inline Object boolObject(int b)
{
    Object o = newObject(TYPE_BOOL);
    o.number = b != 0;
    return o;
}

inline Object nullTerminated(const char* string)
{
    return stringFromSlice(string, strlen(string));
}

inline Object stringFromSlice(const char* string, int len)
{
    Object object = symFromSlice(string, len);
    object.type = TYPE_STRING;
    return object;
}

inline Object escapedStringFromSlice(const char* string, int len)
{
#ifdef STANDALONE
    Object o = withLen(len, TYPE_STRING);
    int c = 0;
    for (int i = 0; i < len; i++) {
        if (string[i] == '\\') {
            i++;
            switch (string[i]) {
                case 'n':
                    o.string[c] = '\n';
                    break;
                default:
                    o.string[c] = string[i];
                    break;
            }
        } else {
            o.string[c] = string[i];
        }
        c++;
    }
    o.string[c] = '\0';
    return o;
#else
    Object o = symFromSlice(string, len);
    o.type = TYPE_STRING;
    return o;
#endif
}

inline Object symFromSlice(const char* string, int len)
{
    Object o = withLen(len, TYPE_SYMBOL);
    snprintf(o.string, len + 1, "%s", string);
    return o;
}

inline Object withLen(size_t len, enum Type type)
{
    Object o = newObject(type);
    o.string = malloc(sizeof(char) * (len + 2));
    o.string[0] = 1;
    o.string += 1;
    return o;
}

inline Object constructLambda(const Object* params, const Object* body, struct Environment* env)
{
    if (!params || !body) {
        throw(NULL_LAMBDA_LIST, "fn params and body cannot be null");
    }

    if (params->type != TYPE_LIST) {
        throw(LAMBDA_ARGS_NOT_LIST, "fn params must be TYPE_LIST, but is %s", getTypeName(params));
    }

    const Object* docs = body;
    size_t docLength = 0;
    while (body->type == TYPE_STRING) {
        docLength++;
        body = body->forward;
    }

    if (body->type != TYPE_LIST) {
        throw(LAMBDA_ARGS_NOT_LIST, "fn body must be TYPE_LIST, but is %s", getTypeName(body));
    }

    Object o = newObject(TYPE_LAMBDA);
    o.lambda = malloc(sizeof(struct Lambda));
    o.lambda->params = cloneList(params);
    o.lambda->body = cloneList(body);
    lambdaRefs(&o) = 1;

    if (docLength == 0) {
        lambdaDocs(&o) = NULL;
    } else {
        lambdaDocs(&o) = malloc(sizeof(char*) * (docLength + 1));
        char** docTexts = lambdaDocs(&o);
        while (docs->type == TYPE_STRING) {
            *docTexts = strdup(docs->string);
            docs = docs->forward;
            docTexts++;
        }
        *docTexts = NULL;
    }

    Object* dest = &o.lambda->body;
    FOR_POINTER_IN_LIST(dest) {
        if (POINTER->type == TYPE_SYMBOL) {
            Object fetched = fetchFromEnvironment(POINTER->string, env);
            // TODO: Figure out why lambdas in particular break when doing this.
            if (!isError(fetched, DID_NOT_FIND_SYMBOL) && fetched.type != TYPE_LAMBDA) {
                fetched.forward = POINTER->forward;
                cleanObject(POINTER);
                *POINTER = fetched;
            } else {
                cleanObject(&fetched);
            }
        }
    }
    return o;
}

inline int isError(const Object obj, const enum errorCode err)
{
    return obj.type == TYPE_ERROR && getErrorCode(obj) == err;
}

inline int bothAre(const enum Type type, const Object* obj1, const Object* obj2)
{
    return (obj1->type == type) && (obj2->type == type);
}

inline int areSameType(const Object* obj1, const Object* obj2)
{
    return obj1->type == obj2->type;
}

inline Object otherObject()
{
    Object o = newObject(TYPE_OTHER);
    o.other = malloc(sizeof(struct Other));
    return o;
}

inline Object errorObject(enum errorCode err)
{
    Object o = newObject(TYPE_ERROR);
#ifdef SIMPLE_ERRORS
    o.error = err;
#else
    o.error = malloc(sizeof(struct Error));
    o.error->code = err;
    o.error->context = NULL;
    o.error->plContext = NULL;
#endif

    return o;
}

#ifndef SIMPLE_ERRORS

inline Object
errorWithAllocatedContextLineNo(enum errorCode code, char* context, int lineNo, const char* fileName)
{
    Object o = errorObject(code);
    sprintf(context + strlen(context), " [33m%s:%d[0m", fileName, lineNo);
    o.error->context = context;
    return o;
}

inline Object errorWithContextLineNo(enum errorCode code, const char* context, int lineNo, const char* fileName)
{
    if (!context) {
        context = "";
    }
    char* copiedContext = malloc(sizeof(char) * ERR_LEN);
    strcpy(copiedContext, context);
    return errorWithAllocatedContextLineNo(code, copiedContext, lineNo, fileName);
}

#endif

struct Error noError()
{
    struct Error err;
    err.context = NULL;
    return err;
}
