#include "pebbleobject.h"

#include "../pebblemain.h"
#include "../object.h"

Object pebbleOther(enum PebbleType type, void* data, void (* cleanup)(Object*),
                   Object (* clone)(struct Other*))
{
    struct Object o = otherObject();
    struct PebbleObject* po = malloc(sizeof(struct PebbleObject));
    o.other->data = po;
    o.other->cleanup = cleanup;
    o.other->clone = clone;
    po->type = type;
    po->ptr = data;
    return o;
}

enum PebbleType getPebbleType(const Object o1)
{
    if (o1.type == TYPE_OTHER) {
        struct PebbleObject* po = o1.other->data;
        enum PebbleType t = po->type;
        return t;
    }
    return P_ERROR;
}

struct PebbleObject* accessPebbleObject(Object obj)
{
    if (getPebbleType(obj) != P_ERROR) {
        return obj.other->data;
    }
    return NULL;
}

void custom_window_load(Window* window)
{
}

void custom_window_unload(Window* window)
{
    window_destroy(window);
}

Object createWindow(Object* params, int length, struct Environment* env)
{
    Window* window = window_create();
    WindowHandlers wh = {.load = custom_window_load,
        .unload = custom_window_unload};
    window_set_window_handlers(window, wh);
    return pebbleOther(WINDOW, window, NULL, NULL);
}

Object deleteWindow(Object* params, int length, struct Environment* env)
{
    Object window = params[0];
    /* Maintain a list of layers to delete? */
    if (getPebbleType(window) == WINDOW) {
        window_stack_remove(accessPebbleObject(window)->window, true);
        return trueObject();
    }
    return falseObject();
}

Object pushWindow(Object* params, int length, struct Environment* env)
{
    Object window = params[0];
    if (getPebbleType(window) == WINDOW) {
        window_stack_push(accessPebbleObject(window)->window, true);
        return trueObject();
    }
    return falseObject();
}

Object updateTextLayer(Object* params, int length, struct Environment* env)
{
    Object textLayer = params[0];
    Object text = params[1];
    if (getPebbleType(textLayer) == TEXT_LAYER) {
        struct PebbleObject* po = accessPebbleObject(textLayer);
        size_t l;
        char* string = stringObj(&text, &l);
        snprintf(po->textLayer->text, 999, "%s", string);
        free(string);
        text_layer_set_text(po->textLayer->layer, po->textLayer->text);
        return trueObject();
    }
    return falseObject();
}

Object addTextLayer(Object* params, int length, struct Environment* env)
{
    Object window = params[0];
    Object text = params[1];
    if (getPebbleType(window) != WINDOW) {
        throw(BAD_TYPE, "Expected a pebble window, but received %s", getTypeName(&window));
    }
    Layer* window_layer =
        window_get_root_layer(accessPebbleObject(window)->window);
    GRect bounds = layer_get_bounds(window_layer);

    struct PLTextLayer* textLayer = malloc(sizeof(struct PLTextLayer));
    textLayer->text = calloc(sizeof(char), 100);

    textLayer->layer = text_layer_create(bounds);
    size_t l;
    char* string = stringObj(&text, &l);
    snprintf(textLayer->text, 999, "%s", string);
    free(string);
    text_layer_set_text(textLayer->layer, textLayer->text);
    text_layer_set_font(textLayer->layer,
                        fonts_get_system_font(FONT_KEY_BITHAM_42_BOLD));
    layer_add_child(window_layer, text_layer_get_layer(textLayer->layer));

    return pebbleOther(TEXT_LAYER, textLayer, NULL, NULL);
}

Object subscription;
struct Environment* subscriptionEnv;

static void subscriptionHandler(struct tm* tick_time, TimeUnits changed)
{
    eval(&subscription, subscriptionEnv);
}

int getUnit(Object time)
{
    if (time.type != TYPE_NUMBER) {
        return MINUTE_UNIT;
    }
    switch (time.number) {
        case 1:
            return SECOND_UNIT;
        case 2:
            return MINUTE_UNIT;
        case 3:
            return HOUR_UNIT;
        case 4:
            return DAY_UNIT;
        case 5:
            return MONTH_UNIT;
        case 6:
            return YEAR_UNIT;
        default:
            return MINUTE_UNIT;
    }
}

Object subscribe(Object* params, int length, struct Environment* env)
{
    Object subscription = cloneObject(params[1]);
    if (subscription.type == TYPE_LAMBDA) {
        int unit = getUnit(params[0]);
        subscriptionEnv = env;
        tick_timer_service_subscribe(unit, subscriptionHandler);
        return trueObject();
    } else {
        printf("Is not lambda, is %d\n", subscription.type);
    }
    return falseObject();
}

Object doVibe(Object* params, int length, struct Environment* env)
{
    Object patternList = params[0];
    int l = listLength(&patternList);
    uint32_t pattern[l];
    if (l > 0) {
        int i = 0;
        Object* pl = &patternList;
        FOR_POINTER_IN_LIST(pl) {
            if (POINTER->type == TYPE_NUMBER) {
                pattern[i] = POINTER->number;
            }
            i++;
        }
        vibes_enqueue_custom_pattern(
            (VibePattern) {.durations = pattern, .num_segments = l});
        return trueObject();
    } else {
        throw(NOT_A_LIST, "(vibe) requires a non-empty list!");
    }
}
