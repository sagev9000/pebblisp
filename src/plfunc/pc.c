#ifdef STANDALONE
#include "pc.h"

#include <unistd.h>
#include <string.h>

Object print(Object* params, int length, unused struct Environment* env)
{
    for (int i = 0; i < length; i++) {
        _printObj(&params[i], 0);
    }
    return numberObject(0);
}

Object typeOf(Object* params, unused int length, unused struct Environment* env)
{
    const char* typeString = getTypeName(&(params[0]));
    return nullTerminated(typeString);
}

Object numToChar(Object* params, unused int length, unused struct Environment* env)
{
    checkTypes(numToChar);
    Object c = params[0];

    if (c.number > 255 || c.number < 0) {
        throw(BAD_NUMBER, "Char values should be between 0 and 255 (inclusive), but received %ld", c.number);
    }
    char ch[1] = { c.number };
    return stringFromSlice(ch, 1);
}

Object takeInput(Object* params, int length, unused struct Environment* env)
{
    Object prompt = params[0];
    if (length > 0 && prompt.type == TYPE_STRING) {
        printf("%s", prompt.string);
    }
    char input[256] = "";
    if (fgets(input, 256, stdin)) {
        return nullTerminated(input);
    }
    return errorWithContext(NULL_PARSE, "fgets() error");
}

Object cd(Object* params, unused int length, unused struct Environment* env)
{
    checkTypes(cd);

    return numberObject(chdir(params[0].string));
}

Object cwd(unused Object* params, unused int length, unused struct Environment* env)
{
    char c[128];
    return nullTerminated(getcwd(c, sizeof(c)));
}

Object systemCall(Object* params, unused int length, unused struct Environment* env)
{
    checkTypes(systemCall);
    Object process = params[0];

    if (isStringy(process)) {
        return numberObject(system(process.string));
    }
    return numberObject(255);
}

Object readFileToObject(Object* params, unused int length, unused struct Environment* env)
{
    checkTypes(readFileToObject);
    Object filename = params[0];

    FILE* file = fopen(filename.string, "r");
    if (!file) {
        throw(NULL_PARSE, "Error opening file at %s", filename.string);
    }

    Object string = newObject(TYPE_STRING);
    string.string = readFileToString(file);
    fclose(file);
    return string;
}

Object getEnvVar(Object* params, unused int length, unused struct Environment* env)
{
    checkTypes(getEnvVar);
    const char* envVar = getenv(params[0].string);
    if (envVar) {
        return nullTerminated(envVar);
    }
    return stringFromSlice("", 0);
}

#include <time.h>
int initializedRand = 0;
Object randomO(Object* params, unused int length, unused struct Environment* env)
{
    if (!initializedRand) {
        srand(time(0));
        initializedRand = 1;
    }
    int num = rand();
    if (length > 0 && params[0].type == TYPE_NUMBER) {
        num = num % params[0].number;
    }
    return numberObject(num);
}

#endif // STANDALONE