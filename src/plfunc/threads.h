#ifdef STANDALONE

#ifndef THREADS_H
#define THREADS_H

#include "../pebblisp.h"

Object clonePromise(Object src);

void cleanPromise(struct Promise* promise);

int isDone(struct Promise* promise);

int isPromise(Object src);

tfn(async, "async",
    ({ expect(isFuncy), returns(isPromise)}),
    "Run the given lambda on a separate thread, returning a promise.",
// Disabled because the test ends while memory is still allocated, causing valgrind to report errors
    ";(def sleepy (fn () ((sys \"sleep 0.02\") \"Hiya\"))) (def x (async sleepy)) x", "=> <PENDING>",
    "(def sleepy (fn () ((sys \"sleep 0.02\") \"Hiya\"))) (def x (async sleepy)) (await x)", "Hiya",
);

tfn(await, "await",
    ({ expect(isPromise), anyType }),
    "Waits for a promise to resolve before proceeding.\n"
    "Note: Calling (await) on the same object multiple times is undefined behavior.",
    "(def sleepy (fn () ((sys \"sleep 0.02\") \"Hiya\"))) (def x (async sleepy)) (await x)", "Hiya",
);

#endif // PEBBLISP_THREADS_H

#endif // STANDALONE