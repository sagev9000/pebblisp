#ifndef PEBBLISP_GENERAL_H
#define PEBBLISP_GENERAL_H

#include "../pebblisp.h"

fn(add, "+",
   "Add numbers",
   "(+ 1 2)", "3",
   "(+ 1 2 3 4)", "10",
);

fn(sub, "-",
   "Subtract numbers",
   "(- 3 2)", "1",
   "(- 10 3 1)", "6",
);

fn(mul, "*",
   "Multiply numbers",
   "(* 3 2)", "6",
   "(* 1 2 3 4)", "24",
);

fn(dvi, "/",
   "Divide numbers",
   "(/ 8 2)", "4",
   "(/ 100 5 2)", "10",
);

fn(mod, "%",
   "Get the modulus of two numbers",
   "(% 10 3)", "1",
);

fn(equ, "=",
   "Check if two values are equal",
   "(= (+ 2 2) 4)", "T",
   "(= \"Hello\" (cat \"He\" \"llo\"))", "T",
   "(= (1 2 3) (1 2 3))", "T",
   "(= 10 \"10\")", "F",
   "(= 2 5)", "F",
);

fn(greaterThan, ">",
   "Check if one number is greater than another",
   "(> 10 3)", "T",
   "(> 2 5)", "F",
);

fn(lessThan, "<",
   "Check if one number is less than another",
   "(< 2 5)", "T",
   "(< 10 3)", "F",
);

fn(and, "&",
   "Check if all conditions are true",
   "(& T T)", "T",
   "(& F T)", "F",
   "(& T T T T T F)", "F",
);

fn(or, "|",
   "Check if any conditions are true",
   "(| T F)", "T",
   "(| F F)", "F",
   "(| F F F F F T)", "T",
);

tfn(catObjects, "cat",
    ({ anyType, returns(isStringy) }),
    "Concatenate string representations of the given objects.",
    "(cat \"Stuff: \" (1 2 3))", "Stuff: ( 1 2 3 )",
);

tfn(filter, "fil",
    ({ expect(isFuncy), expect(isListy), returns(isListy) }),
    "Filter a list based on the given condition.",
    "(fil (fn (a) (< 50 a)) (25 60 100))", "( 60 100 )",
    "(fil (fn (a) (< 0 (len a))) ( () (1) (1 2) () ))", "( ( 1 ) ( 1 2 ) )",
);

tfn(append, "ap",
    ({ expect(isListy), anyType, returns(isListy) }),
    "Append the given element. Creates a new list.",
    "(ap (1 2) 3)", "( 1 2 3 )",
);

tfn(prepend, "pre",
    ({ expect(isListy), anyType, returns(isListy) }),
    "Prepend the given element. Creates a new list",
    "(pre (2 3) 1)", "( 1 2 3 )",
);

tfn(len, "len",
    ({ expect(isListy), returns(isNumber) }),
    "Returns the length of the given list, or an error if the expression is not a list.",
    "(len (2 3))", "2",
    "(len ())", "0",
    "(len \"string\")", "BAD_PARAMS;",
);

tfn(reduce, "reduce",
    ({ anyType, expect(isFuncy), anyType, anyType }),
    "Performs a simple reduction.\n"
    "Takes three arguments:\n"
    " - Values\n"
    " - A function to apply to each value\n"
    " - An initial value",
    "(reduce 5 + 6)", "11",
    "(reduce (1 2 3) + 0)", "6",
    "(def sum (fn (left right) ((+ left right)) )) (reduce 5 sum 6)", "11",
    "(def sum (fn (left right) ((+ left right)) )) (reduce (1 2 3) sum 0)", "6",
);

tfn(at, "at",
    ({ expect(isNumber), expect(isListy), anyType }),
    "Get item at the given index in the given list.",
    "(at 1 (1 2 3))", "2",
    "(at 99 (1 2 3))", "INDEX_PAST_END",
    "(at 99 \"string\")", "BAD_PARAMS;",
);

tfn(rest, "rest",
    ({ expect(isListy), returns(isListy) }),
    "Get the tail of a list. All but the first element.",
    "(rest (1 2 3))", "( 2 3 )",
    "(rest ())", "( )",
    "(rest \"string\")", "BAD_PARAMS;",
);

tfn(reverse, "rev",
    ({ expect(isListy), returns(isListy) }),
    "Reverse a list.",
    "(rev (1 2 3))", "( 3 2 1 )",
    "(rev \"string\")", "BAD_PARAMS;",
);

tfn(isNum, "isnum",
    ({ anyType, returns(isBool) }),
    "Returns `T` only if the argument evaluates to a number.",
    "(isnum 1)", "T",
    "(isnum (+ 5 5))", "T",
    "(isnum '(+ 5 5))", "F",
    "(isnum \"Hello\")", "F",
);

tfn(isList, "islist",
    ({ anyType, returns(isBool) }),
    "Returns `T` only if the argument is a list.",
    "(islist (1 2 3))", "T",
    "(islist ())", "T",
    "(islist \"Stringy\")", "F",
);

tfn(isString, "isstr",
    ({ anyType, returns(isBool) }),
    "Returns `T` only if the argument is a string.",
    "(isstr \"Heyo\")", "T",
    "(isstr \"\")", "T",
    "(isstr (cat 5 5))", "T",
    "(isstr 10)", "F",
);

tfn(isErr, "iserr",
    ({ anyType, returns(isBool) }),
    "Check if the argument is an error.",
    "(iserr (at 10 ()))", "T",
    "(iserr 5)", "F",
);

/// STRING/SLIST => ANY
fn(parseEvalO, "eval",
   "Evaluate the given string or quoted list. Uses the global scope.",
   "(eval \"(1 2 3)\")", "( 1 2 3 )",
   "(eval '(+ 5 5))", "10",
);

tfn(getTime, "time",
    ({ returns(isStruct) }),
    "Get a struct of the current time with fields (minute hour sec)."
);

#endif // PEBBLISP_GENERAL_H
