#ifdef STANDALONE

#ifndef PEBBLISP_PC_H
#define PEBBLISP_PC_H

#include "../pebblisp.h"

fn(print, "prn", "Prints the string representation of the given object to stdout.");

tfn(typeOf, "type-of",
    ({ anyType, returns(isStringy) }),
    "Gets a string indicating the type of the given object",
    "(type-of 10)", "TYPE_NUMBER",
    "(type-of \"string\")", "TYPE_STRING",
    "(type-of (fn () ()))", "TYPE_LAMBDA",
);

tfn(numToChar, "ch",
    ({ expect(isNumber), returns(isStringy) }),
    "Gets a string containing the ascii character for the given number value.",
    "(ch 107)", "k",
    "(ch 0x21)", "!",
);

fn(printEnvO, "penv",
   "Prints out the current scoped environment.\n"
   "(penv) prints a mostly human-readable list of env variables.\n"
   "(penv T) prints a mostly list of env variables, including pointer addresses.\n"
   "Calling (penv) with no argument is equivalent to (penv F)"
);

tfn(systemCall, "sys",
    ({ expect(isStringy), returns(isNumber) }),
    "Opens a shell and runs the given command, returning 0 if successful.\n"
    "If the argument is not a string, returns 255.\n",
    "(sys \"echo yee > /dev/null\")", "0",
);

tfn(cd, "cd",
    ({ expect(isStringy), anyType }),
    "Change the current directory.",
    "(cd \"/\") (cwd)", "/"
);

tfn(cwd, "cwd",
    ({ returns(isStringy) }),
    "Get the current directory.",
    "(cd \"/\") (cwd)", "/"
);

/// @code
/// () => STRING
/// STRING => STRING
fn(takeInput, "inp",
   "Take console input with an optional prompt. For example:\n"
   "`(def x (input))` will wait for user input with no prompt.\n"
   "`(def x (input \">> \"))` wait for input, but prompt the user with '>> '\n"
);

tfn(readFileToObject, "rf",
    ({ expect(isStringy), returns(isStringy) }),
    "Read a file into a string object."
);

tfn(getEnvVar, "env",
    ({ expect(isStringy), returns(isStringy) }),
    "Get a variable from the current environment\n"
    "(env HOME) => /home/sagevaillancourt"
);

tfn(randomO, "rand",
    ({ returns(isNumber) }),
    "Returns a semi-random integer\n"
    "(rand) => 2394568"
);

#endif //PEBBLISP_PC_H

#endif // STANDALONE
