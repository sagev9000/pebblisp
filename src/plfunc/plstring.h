#include "../pebblisp.h"

tfn(charAt, "chat",
    ({ expect(isStringy), expect(isNumber), returns(isStringy) }),
    "Get the char in the given string at the given index.",
    "(chat \"Hello\" 1)", "e",
    "(chat \"Hello\" 10)", "",
);

tfn(charVal, "char",
    ({ expect(isStringy), returns(isNumber) }),
    "Get the ascii integer representaton of the given character.",
    "(char \"h\")", "104",
    "(char \"hello\")", "104",
    "(char \"\")", "0",
);

tfn(slen, "slen",
    ({ expect(isStringy), returns(isNumber) }),
    "Returns the length of the given string",
    "(slen \"string\")", "6",
    "(slen \"\")", "0",
);

tfn(chars, "chars",
    ({ expect(isStringy), returns(isListy) }),
    "Get a list of all chars in the given string",
    "(chars \"hello\")", "( h e l l o )",
    "(chars \"\")", "( )",
);

tfn(matches, "matches",
    ({ expect(isStringy), expect(isStringy), returns(isBool) }),
    "Check that a string matches a basic wildcard pattern\n"
    "Note: Currently there is no way to match a literal asterisk",
    "(matches \"Hiya\" \"Hiya\")", "T",
    "(matches \"Howdy\" \"H*y\")", "T",
    "(matches \"Yello\" \"*\")", "T",
    "(matches \"S\" \"*S\")", "T",
    "(matches \"S\" \"S*\")", "T",
    "(matches \"\" \"*\")", "T",
    "(matches \"ThisIsALongerOne\" \"This*ALong*One\")", "T",
    "(matches \"ThisIsALongerOne\" \"*This*ALong*One*\")", "T",
    ";", "",
    "(matches \"Howdy\" \"*llo\")", "F",
    "(matches \"Stink\" \"Stank\")", "F",
);

tfn(substring, "substr",
    ({ expect(isNumber), expect(isNumber), expect(isStringy), returns(isStringy) }),
    "Get a substring from the given string.",
    "(substr 1 3 \"Hello\")", "el",
    "(substr 99 3 \"Hello\")", "BAD_PARAMS;",
    "(substr 98 99 \"Hello\")", "BAD_PARAMS;",
    "(substr 0 1 \"\")", "",
    "(substr 0 99 \"Hey\")", "Hey",
    "(substr 1 99 \"Heyyyy\")", "eyyyy",
);
