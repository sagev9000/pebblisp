#ifdef STANDALONE

#include "../pebblisp.h"

fn(startServer, "serve",
   "(serve) => 0\n"
   "Starts a simple web server with routes that have been added using (get) and (post).\n"
   "Returns 0 if the server was successfully started, otherwise 1.\n"
   "Note: Not a blocking call! Calling (inp) is a simple way to keep the server open.\n"
);

fn(addGetRoute, "get",
   "Adds a GET route at the given path with the given function.\n"
   "  (get \"/\" (fn () (\"Hello, world!\")))\n"
   "  (get \"/parampath\" (fn (req) (req's queryParams)))\n"
   "  (serve)\n"
   "Also see: (serve)\n"
);

fn(addPostRoute, "post",
   "Adds a POST route at the given path with the given function.\n"
   "Note: Can't do anything with POSTed data yet.\n"
   "  (post \"/\" (fn () (\"Hello, world!\")))\n"
   "  (serve)\n"
   "Also see: (serve)\n"
);

#endif
