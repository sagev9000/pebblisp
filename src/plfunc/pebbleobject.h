#include <pebble.h>

#include "../pebblisp.h"

enum PebbleType {
    WINDOW, TEXT_LAYER, P_ERROR
};

struct PLTextLayer {
    TextLayer* layer;
    char* text;
};

typedef struct PebbleObject PebbleObject;
struct PebbleObject {
    enum PebbleType type;
    union {
        Window* window;
        struct PLTextLayer* textLayer;
        void* ptr;
    };
};

Object createWindow(Object* params, int length, struct Environment* env);

Object deleteWindow(Object* params, int length, struct Environment* env);

Object pushWindow(Object* params, int length, struct Environment* env);

Object addTextLayer(Object* params, int length, struct Environment* env);

Object updateTextLayer(Object* params, int length, struct Environment* env);

Object subscribe(Object* params, int length, struct Environment* env);

Object doVibe(Object* params, int length, struct Environment* env);
