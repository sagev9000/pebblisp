#include "tokens.h"

#include <string.h>

/*
 * Grammar:
 *   Number:
 *     Hex:
 *       0x[0-9a-f]+
 *     Bin:
 *       0b[01]+
 *     Dec:
 *       -?[0-9]+
 *
 *   String:
 *      "([^\n"])|(\\")*"
 *      """.*"""
 *
 *   Boolean:
 *     T
 *     F
 *
 *   List:
 *     (...Expression)
 *
 *   SList:
 *     '(...Expression)
 *
 *   Symbol:
 *     [a-z][a-z0-9]*
 *
 *   Primitive:
 *     Symbol
 *     Number
 *     String
 *     Boolean
 *     List
 *
 *   Function Call:
 *     (Symbol ...Expression)
 *
 *   Struct Access:
 *     Symbol.Symbol
 *
 *   Expression:
 *     Primitive
 *     Function Call
 *     Struct Access
 */

// Is the char a standalone token?
int isSingle(const char *c)
{
    static const char singleTokens[] = "()'?";

    int i = 0;
    while (singleTokens[i] != '\0') {
        if (singleTokens[i] == c[0]) {
            return 1;
        }
        i++;
    }
    // It's not a single if it's next to another '.'
    if (c[0] == '.' && c[1] != '.' && c[-1] != '.') {
        return 1;
    }
    return 0;
}

int isDigit(const char c)
{
    return c >= '0' && c <= '9';
}

int isWhitespace(const char c)
{
    return c == ' ' || c == '\t' || c == '\n';
}

void buildErrFromInput(struct Error* err, enum errorCode code, int i, const char* input, struct Slice* slice)
{
    err->context = malloc(sizeof(char) * ERR_LEN + 1);
    err->code = code;
    err->plContext = slice;
    int start = i > ERR_LEN ? i - ERR_LEN : 0;
    strncpy(err->context, &input[start], ERR_LEN);
}

void collectSymbol(const char* input, int* i, int* length);

void processString(const char* input, struct Error* err, struct Slice* slice, int* i, int* lineNumber, int* length);

struct Slice* nf_tokenize(const char* input, struct Error* err)
{
    if (!input) {
        err->context = strdup("no input");
        return NULL;
    }

    struct Slice slices[MAX_TOK_CNT];
    struct Slice* slice = &slices[0];
    //int token_count = MAX_TOK_CNT;
    // do {
    //     slices = malloc(sizeof(struct Slice) * token_count);
    //     token_count /= 2;
    // } while (slices == NULL);

    int i = 0;
    int lineNumber = 1;

    int parens = 0;
    while (input[i] != '\0') {
        int length = 1;
        if (isWhitespace(input[i])) {
            if (input[i] == '\n') {
                lineNumber++;
            }
            i++;
            continue;
        }

        if (input[i] == '(') {
            parens++;
        } else if (input[i] == ')') {
            parens--;
            if (parens < 0) {
                buildErrFromInput(err, MISMATCHED_PARENS, i, input, slice);
                //free(slices);
                return NULL;
            }
        }

        slice->text = &input[i];
        slice->lineNumber = lineNumber;

        if (isSingle(&input[i])) {
            i++;
        } else if (input[i] == ';') {
            while (input[i] && input[i] != '\n') {
                i++;
            }
            continue;
        } else if (input[i] == '"') {
            processString(input, err, slice, &i, &lineNumber, &length);
            if (err->context != NULL) {
                return NULL;
            }
        } else {
            collectSymbol(input, &i, &length);
            if (length == 0) {
                buildErrFromInput(err, BAD_SYMBOL, i, input, slice);
                return NULL;
            }
        }

        slice->length = length;
        slice++;
    }

    if (parens != 0) {
        buildErrFromInput(err, MISMATCHED_PARENS, i, input, slice);
        //free(slices);
        return NULL;
    }

    slice->text = NULL;
    slice->length = 0;
    size_t size = sizeof(struct Slice) * ((slice - &slices[0]) + 1);
    struct Slice* allocated = malloc(size);
    memcpy(allocated, slices, size);

    return allocated;
}

void singleQuotedString(const char* input, int* i, int* lineNumber, int* length);

void tripleQuotedString(const char* input, struct Error* err, struct Slice* slice, int* i, int* lineNumber,
                        int* length);

void processString(const char* input, struct Error* err, struct Slice* slice, int* i, int* lineNumber, int* length)
{
    if (input[(*i) + 1] == '"' && input[(*i) + 2] == '"') {
        tripleQuotedString(input, err, slice, i, lineNumber, length);
    } else {
        singleQuotedString(input, i, lineNumber, length);
    }
    (*i)++;
}

int validSymbolChar(const char* c)
{
    return !isWhitespace(*c)
        && !isSingle(c)
        && *c != '"'
        && *c != '\0';
}

void collectSymbol(const char* input, int* i, int* length)
{
    // TODO: Error if length is 0?
    while (validSymbolChar(&input[++(*i)])) {
        (*length)++;
    }
}

void tripleQuotedString(const char* input, struct Error* err, struct Slice* slice, int* i, int* lineNumber, int* length)
{
    // Skip past the extra opening quotes
    (*i) += 2;
    slice->text += 2;

    for (;;) {
        (*i)++;
        const int c = *i;
        if (input[c] == '"' && input[c + 1] == '"' && input[c + 2] == '"') {
            break;
        }
        if (input[c] == '\n') {
            (*lineNumber)++;
        }
        (*length)++;
        if (input[c] == '\0' || input[c + 1] == '\0' || input[c + 2] == '\0') {
            buildErrFromInput(err, UNEXPECTED_EOF, c, input, slice);
            return;
        }
    }
}

void singleQuotedString(const char* input, int* i, int* lineNumber, int* length)
{
    while (input[++(*i)] != '\0') {
        const int c = *i;
        if (input[c] == '"') {
            int backslashes = 0;
            while (input[(c - 1) - backslashes] == '\\') {
                backslashes++;
            }
            // \\\" => Odd number of backslashes, quote IS escaped
            // \\" => Even number of backslashes, quote is NOT escaped
            if (backslashes % 2 == 0) {
                break;
            }
        } else if (input[c] == '\n') {
            (*lineNumber)++;
        }
        (*length)++;
    }
}
