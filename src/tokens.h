#ifndef TOKENS_H
#define TOKENS_H

#include "pebblisp.h"

int isSingle(const char *c);

int isDigit(char c);

int isHex(char c);

struct Slice* nf_tokenize(const char* input, struct Error* err);

#endif
