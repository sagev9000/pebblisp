#ifndef PEBBLE_MAIN_H
#define PEBBLE_MAIN_H

#include <pebble.h>
#include "pebblisp.h"

#define SMAX_LENGTH 256
#define CELL_HEIGHT 44

#define SCRIPT_COUNT 5

void debug_result(const char* d);

#endif
