#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include "object.h"
#include "hash.h"

struct StrippedObject {
    Type type;
    void* data;
};

struct EnvElement {
    char* symbol;
    struct StrippedObject object;
};

struct Environment;

struct Environment {
    struct Environment* outer;
    struct ObjectTable table;
    int refs;
};

Object deStrip(struct StrippedObject object);

struct Environment* global();

void setGlobal(struct Environment* env);

Object fetchFromEnvironment(const char* name, struct Environment* env);

struct Environment envForLambda(const Object* params, const Object* arguments, int paramCount,
                                struct Environment* outer);

void addToEnv(struct Environment* env, const char* name, Object obj);

void setInEnv(struct Environment* env, const char* name, Object obj);

void printEnv(struct Environment* env, int printPointers);

void addFunc(const char* name,
             Object (* func)(Object*, int, struct Environment*),
             struct Environment* env);

void deleteEnv(struct Environment* e);

void shredDictionary();

struct Environment defaultEnv();

struct Environment defaultEnvPreAllocated(struct EnvElement* elements);

int getStructIndex(const char* name);

struct StructDef* getStructAt(int i);

void addStructDef(struct StructDef def);

void printColored(const char* code);

int runTests(int detailed, int specificTest);

fn(segfault, "seg",
   "Induces a segfault."
);

fn(help, "?",
   "Gets a string with help text or a string representation for the object.\n"
   "\n"
   " Function help:\n"
   "   (? islist) => \"(islist (1 2 3)) => T\"\n"
   "\n"
   " Struct fields:\n"
   "   (? Output) => \"{ stdout stderr }\"\n"
   "\n"
   " Other objects:\n"
   "   (? \"Hello\") => \"Hello\"\n"
   "\n"
   "Note: The included repl allows you to drop the parentheses."
);


#endif
