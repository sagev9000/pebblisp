#!/usr/bin/pl

(struct Post
  (title body))

(def element (fn (type)
  (fn (text)
    (cat "<" type ">"
      (if (islist text) (reduce text cat "") (reduce (text) cat ""))
      "</" type ">"))))

(def html (element "html"))
(def body (element "body"))
(def head (element "head"))
(def h1 (element "h1"))
(def h2 (element "h2"))
(def p (element "p"))
(def div (element "div"))
(def article (element "article"))

(def singleton (fn (type) (fn (text) (cat "<" type " " (reduce text cat "") ">"))))
(def link (singleton "link"))

(def attribute (fn (type) (fn (value) (cat type "='" value "'"))))
(def rel (attribute "rel"))
(def href (attribute "href"))

(def htmlize (fn (post) (cat
  (h2 post's title)
  (p post's body))))

(def p1 (Post "Hey" "This is a post"))
(def p2 (Post "This"
"Is ALSO a post. And frankly, what a great post!  It's so long and flowing,
and the interpreter won't even instantly crash over it! It's truly astounding
stuff, when you think about it."
))

(def styleHead (head (
  (link ((rel "stylesheet") (href "styles.css")))
)))

(def homepage (fn (req) (html (
  styleHead
  (body (
    (h1 "This is a sweet PebbLisp site")
    (p (cat "" req))
    (htmlize p1)
    (htmlize p2)))
  ))))

(get "/" homepage)
(get "/x" (fn (req) (cat "" req)))

(def styles (rf "styles.css"))
(get "/styles.css" (fn () (styles)))

(def PORT 9090)
(serve PORT)

(prnl (cat "Hosting server on " PORT ". Entering simple REPL. q to quit"))
(def repl (fn () (
  (def input (inp "webby>> "))
  (if (= input "q") () (
    (eval input)
    (repl)
  ))
)))

(repl)
